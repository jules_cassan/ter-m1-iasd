# Imports des librairies
import base64
import re
import sys
import time
import pickle
import string
import warnings

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import nltk
import spacy
from bs4 import BeautifulSoup
from scipy.stats import randint
from tabulate import tabulate

import nltk
from nltk import sent_tokenize, word_tokenize, pos_tag
from nltk.corpus import stopwords, wordnet
from nltk.stem import WordNetLemmatizer
from nltk import RegexpParser
from nltk import sent_tokenize

from sklearn.pipeline import Pipeline
from sklearn.base import TransformerMixin
from sklearn.model_selection import (train_test_split, cross_val_score,
                                     KFold, GridSearchCV)
from sklearn.feature_extraction.text import (CountVectorizer, TfidfVectorizer,
                                             TfidfTransformer)
from sklearn.metrics import (confusion_matrix, classification_report,
                             accuracy_score)
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
import sklearn
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

import random
import pandas as pd
from nltk.corpus import wordnet as wn

def print_evaluation_metrics(y_test, y_pred):
    # Calculer les mesures d'évaluation
    emotions = sorted(y_test.unique())
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred, average='macro')
    recall = recall_score(y_test, y_pred, average='macro')
    f1 = f1_score(y_test, y_pred, average='macro')

    # Afficher les mesures d'évaluation
    print("Mesures d'évaluation :")
    print(f"Accuracy : {accuracy:.2f}")
    print(f"Precision : {precision:.2f}")
    print(f"Recall : {recall:.2f}")
    print(f"F1 Score : {f1:.2f}")

    # Calculer et afficher la matrice de confusion
    conf_matrix = confusion_matrix(y_test, y_pred)
    plt.figure(figsize=(10, 8))
    sns.heatmap(conf_matrix, annot=True, fmt='d', cmap='Blues', xticklabels=emotions, yticklabels=emotions)
    plt.xlabel('Prédits')
    plt.ylabel('Vrais')
    plt.title('Matrice de confusion')
    plt.show()



from sklearn.datasets import make_classification
def creation_donnes():
    X, y = make_classification(n_samples=100, n_features=20, n_classes=2, n_clusters_per_class=1, random_state=42)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=22)
    return X_train, X_test, y_train, y_test


def list_predict(list):
  predictions = []
  for data in list:
    emotion = emotion_classifier.predict([data])
    predictions.append(emotion)
  return predictions
  
  
  
  
  
  
def classification(classifier, X_train, y_train, X_test):
    modele = classifier()
    modele.fit(X_train, y_train)
    y_pred = modele.predict(X_test)
    return y_pred



def print_evaluation_metrics_binary(y_test, y_pred):
    # Calculer les mesures d'évaluation
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred)

    # Afficher les mesures d'évaluation
    print("Mesures d'évaluation :")
    print(f"Accuracy : {accuracy:.2f}")
    print(f"Precision : {precision:.2f}")
    print(f"Recall : {recall:.2f}")
    print(f"F1 Score : {f1:.2f}")

    # Calculer et afficher la matrice de confusion
    conf_matrix = confusion_matrix(y_test, y_pred)
    plt.figure(figsize=(10, 8))
    sns.heatmap(conf_matrix, annot=True, fmt='d', cmap='Blues')
    plt.xlabel('Prédits')
    plt.ylabel('Vrais')
    plt.title('Matrice de confusion')
    plt.show()