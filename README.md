# TER 

 La reconnaissance des émotions est un sujet d’une complexité considérable pour
 l’être humain, tout en présentant un potentiel significatif. Que ce soit dans le
 domaine du marketing, où elle peut être utilisée pour analyser les avis sur les
 produits, dans le domaine de la psychologie, ou dans d’autres domaines, cette
 question revêt une importance majeure et pourrait être automatisée.
 Notre projet s’articule autour de la détection automatique des émotions dans les
 conversations textuelles. Notre objectif est de répondre à la problématique suivante :
 Comment détecter les diverses émotions présentes au sein d’une conversation
 textuelle ?
 Pour aborder cette question, nous commencerons par une exploration du fonction
nement des émotions humaines. Nous utiliserons ensuite le modèle de génération de
 texte GPT pour générer des données. Par la suite, nous examinerons les différents
 algorithmes existants pour accomplir cette tâche. Enfin, nous exploiterons ce travail
 de recherche pour développer notre propre approche

# Organisation du projet

Ce projet est structuré en plusieurs fichiers distincts, comprenant les données, le code source, les versions exécutées, ainsi que le rapport du projet.


TER-M1-IASD/
├── TER/  
│   ├── dataset/
│   │   └── data.csv                 
│   └── modele.ipynb
├── TER.pdf
├── Rapport.pdf                 
└── README.md
           

Vous pouvez retrouver ces différents fichiers via les liens ci-dessous :

- [Rapport du Projet](Rapport.pdf)
- [Notebook Exécuté](TER.pdf)
- [Notebook](TER/modele.ipynb)
- [Données](TER/dataset/data.csv)

# Installation

Pour installer ce projet il suffit de suivre les instructions suivantes : 


1. **Cloner le dépôt Git :**

   Clonez le dépôt en utilisant la commande suivante :

   ```bash
   git clone https://gitlab.com/jules_cassan/ter-m1-iasd.git
   cd ter-m1-iasd

2. **Transférer le dépôt sur google drive**

    Une fois le dépôt acquis, il faut le transférer sur google drive afin de pouvoir l'exécuté.


